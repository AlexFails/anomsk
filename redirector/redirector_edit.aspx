﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="redirector_edit.aspx.cs" Inherits="redirector_redirector_edit" %>
<%
    if ((Convert.ToString(HttpContext.Current.Session["loginOK"]) != "1"))
    {
        HttpContext.Current.Response.Redirect("~/index.htm");
    } 
%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <input type="button" value="Neuer Eintrag" onclick="location.href = 'new.aspx'" />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:IRDEConnectionString %>" DeleteCommand="DELETE FROM [URLshortcuts] WHERE [shortcut] = @shortcut" InsertCommand="INSERT INTO [URLshortcuts] ([shortcut], [URL]) VALUES (@shortcut, @URL)" SelectCommand="SELECT * FROM [URLshortcuts] ORDER BY [shortcut]" UpdateCommand="UPDATE [URLshortcuts] SET [URL] = @URL WHERE [shortcut] = @shortcut">
            <DeleteParameters>
                <asp:Parameter Name="shortcut" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="shortcut" Type="String" />
                <asp:Parameter Name="URL" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="URL" Type="String" />
                <asp:Parameter Name="shortcut" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="shortcut" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="shortcut" HeaderText="shortcut" ReadOnly="True" SortExpression="shortcut" />
                <asp:BoundField DataField="URL" HeaderText="URL" SortExpression="URL" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
