﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;

public partial class redirector_new : System.Web.UI.Page
{
    SqlConnection objConn = new SqlConnection();
    string myPath = @"C:\HostingSpaces\admin3\irde.oppidium.de\wwwroot\";

    protected void Page_Load(object sender, EventArgs e)
    {
        objConn.ConnectionString = SqlDataSource1.ConnectionString;
        objConn.Open();
        if (Page.IsPostBack)
        {
            if(doesExist(HttpContext.Current.Request.Form["shortcut"]))
            {
                dummy.Text = "Shortcut oder Verzeichnis existiert bereits.";
            }
            else
            {
                SqlCommand myCommand = new SqlCommand();
                myCommand.Connection = objConn;
                myCommand.CommandText = "INSERT INTO URLshortcuts (shortcut, URL) VALUES ('" + HttpContext.Current.Request.Form["shortcut"].ToString() + "', '" + HttpContext.Current.Request.Form["URL"].ToString() + "')";
                myCommand.ExecuteNonQuery();
                HttpContext.Current.Response.Redirect("redirector_edit.aspx");
            }
        }
        objConn.Close();
    }

    protected bool doesExist(string shortcut)
    {
        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM URLshortcuts WHERE shortcut LIKE '" + shortcut + "'", objConn);
        da.Fill(ds);
        DataTable shortcuts = ds.Tables[0];
        if (shortcuts.Rows.Count == 0 && !Directory.Exists(myPath + shortcut))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}