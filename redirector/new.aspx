﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="new.aspx.cs" Inherits="redirector_new" %>
<%
    if ((Convert.ToString(HttpContext.Current.Session["loginOK"]) != "1"))
    {
        HttpContext.Current.Response.Redirect("~/index.htm");
    } 
%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:IRDEConnectionString %>"></asp:SqlDataSource>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>shortcut:</td>
                <td><asp:TextBox ID="shortcut" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>URL:</td>
                <td><asp:TextBox ID="URL" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td><asp:Button ID="submit" runat="server" Text="Speichern" />&nbsp;<input type="button" value="Abbrechen" onclick="location.href = 'redirector_edit.aspx'" /></td>
            </tr>
        </table>
        <asp:Label ID="dummy" runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>
