﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class redirector_login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Int32 calcPW;
        DateTime d = DateTime.Now.Date;
        calcPW = (d.Year * 10) / d.Month * d.Day;
        if (Page.IsPostBack)
        {
            if (Request.Form["pw"] != calcPW.ToString())
            {
                dummy.Text = "Ungültiges Passwort.";
            }
            else
            {
                HttpContext.Current.Session["loginOK"] = "1";
                Response.Redirect("redirector_edit.aspx");
            }
        }
    }
}