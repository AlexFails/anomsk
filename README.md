# README #

This is an Resistance-only private repo containing the sources of Moscow XM Anomaly site.

# Docs #
* see bit.ly/1SN8TaS

# Troubleshooting #

If you have some troubles, make sure what you do some steps:

* Create an issue;
* Make an screenshot (almost required!);
* Write your device info (at least OS type and version) and browser info (name and / or [User-Agent](https://en.wikipedia.org/wiki/User-Agent) String).